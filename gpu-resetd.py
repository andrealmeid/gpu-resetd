#!/bin/python

# GPLv2 License
# Copyright 2022 Igalia
# Author André Almeida <andrealmeid@igalia.com>

import pyudev
import os, signal
import subprocess
from datetime import datetime

VRAM_LOST = 0x1

if os.getuid():
    print("You should run this as root")
    exit(1)

log_path = "/var/log/gpu-crashd.log"
log_file = open(log_path, "a")

context = pyudev.Context()
monitor = pyudev.Monitor.from_netlink(context)
monitor.filter_by('drm')

for device in iter(monitor.poll, None):
    if device.action == "change" and device.get('RESET'):
        log_file = open(log_path, "a")
        pid = int(device.get('PID'))
        flags = device.get('FLAGS')
        vram_lost = int(flags, 16) & VRAM_LOST

        log_file.write(datetime.now().strftime("[%Y-%m-%d %H:%M:%S]") + " "
                + device.get('DEVNAME') + ": ")

        if pid:
            with open(f'/proc/{pid}/cmdline') as f:
                cmdline = f.read().replace("\0", " ")[:-1]

            if not vram_lost:
                log_file.write("GPU reset by '" + cmdline + "', killing it. FLAGS=" + flags + "\n")
                os.kill(pid, signal.SIGKILL)
            else:
                log_file.write("GPU reset by '" + cmdline + "' and VRAM marked as lost, "
                    "restarting full graphical enviroment. FLAGS=" + flags + "\n")
                os.system("systemctl restart graphical.target")
        else:
            log_file.write("GPU reset by kernel, restarting full graphical enviroment.\n")
            os.system("systemctl restart graphical.target")

        log_file.close()
