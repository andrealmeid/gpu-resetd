# gpu-resetd

Daemon to listen to DRM device's reset. Log them all and take some action:
- If an app has crashed, just kill it.
- If the reset was triggered by a kernel thread or VRAM was marked as lost, it's
safer to reset all graphical system

---

Dependencies:
- pyudev

---

Usage:

```
$ sudo ./gpu-resetd.py
...

$ cat /var/log/gpu-crashd.log 
[2022-11-25 00:00:53] /dev/dri/card0: GPU reset by 'vkcube --c 100', killing it. FLAGS=0x0
[2022-11-25 00:01:16] /dev/dri/card0: GPU reset by kernel, restarting full graphical enviroment.
[2022-11-25 00:02:29] /dev/dri/card0: GPU reset by 'vkcube --c 100' and VRAM marked as lost, restarting full graphical enviroment. FLAGS=0x1
```
